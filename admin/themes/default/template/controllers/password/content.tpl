<div id="login-panel">
	<div id="login-header">
		<h1 class="text-center">
			<img id="logo" src="{$img_dir}prestashop@2x.png" width="123px" height="24px" alt="PrestaShop" />
		</h1>
		<h4 class="text-center text-info">{l s='Your password has expired. Please provide a new password.'}</h4>
	</div>
			{if isset($errors)}
			<div id="error" class="alert alert-danger">
				<h4>
					{if isset($nbErrors) && $nbErrors > 1}
						{l s='There are %d errors.' sprintf=$nbErrors}
					{else}
						{l s='There is %d error.' sprintf=$nbErrors}
					{/if}
				</h4>
				<ol>
					{foreach from=$errors item="error"}
					<li>{$error}</li>
					{/foreach}
				</ol>
			</div>
		{/if}
		{if isset($warningSslMessage)}
		<div class="alert alert-warning">{$warningSslMessage}</div>
		{/if}
	<div id="shop-img"><img src="{$img_dir}preston-login@2x.png" alt="{$shop_name}" width="69.5px" height="118.5px" /></div>
	<div class="flip-container">
		<div class="flipper">
			<div class="front panel">
				<h3 id="shop_name">{$shop_name}</h3>
				
				<form action="#" id="password_form" class="panel-body" method="post">
					<div class="form-group">
						<label class="control-label" for="passwd_old">
							{l s='Current password'}
						</label>
						<input name="passwd_old" type="password" id="passwd_old" class="form-control" tabindex="2" placeholder="&#xf084 {l s='Password'}" />
					</div>
					<div class="form-group">
						<label class="control-label" for="passwd">
							{l s='Password'}
						</label>
						<input name="passwd" type="password" id="passwd" class="form-control" tabindex="2" placeholder="&#xf084 {l s='Password'}" />
					</div>
					<div class="form-group">
						<label class="control-label" for="passwd">
							{l s='Confirm password'}
						</label>
						<input name="passwd_conf" type="password" id="passwd_conf" class="form-control" tabindex="2" placeholder="&#xf084 {l s='Password'}" />
					</div>
					<p class="help-block">{l s="Password should be at least %s characters in length and should include at least one upper case letter, one number, and one special character." sprintf=$adminPasswordLength}</p>

					<div class="form-group row-padding-top">
						<button name="submitPasswordChange" type="submit" tabindex="4" class="btn btn-primary btn-lg btn-block ladda-button" data-style="slide-up" data-spinner-color="white" >
							<span class="ladda-label">
								{l s='Change password'}
							</span>
						</button>
					</div>
					<div class="form-group">
						<div class="checkbox pull-right">
							<label for="passwd_send_email">
								<input name="passwd_send_email" type="checkbox" id="passwd_send_email" value="1" tabindex="3"/>
								{l s='Send new password at email'}
							</label>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>